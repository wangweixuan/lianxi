import { Button, Card, message, Pagination, Skeleton } from 'antd'
import * as React from 'react'
import { getExerciseContents } from '../model/loader-exercise'
import { ExerciseContents } from '../model/model-exercise'
import { LoadedResult, loadingResult, ResultKind } from '../model/model-general'
import { NavController } from '../model/nav'

interface ExerciseViewerProps {
  id: string
  nav: NavController
}

interface ExerciseViewerState {
  contents: LoadedResult<ExerciseContents>
  questionIndex: number
  tabKey: TabKey
}

const enum TabKey {
  Summary = 'summary',
  Answer = 'answer',
  Explanation = 'explanation'
}

export class ExerciseViewerPage extends React.Component<
  ExerciseViewerProps,
  ExerciseViewerState
> {
  constructor(props: ExerciseViewerProps) {
    super(props)
    this.state = {
      contents: loadingResult,
      questionIndex: 1,
      tabKey: TabKey.Answer
    }
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '练习加载中', canBack: true })
    this.getContents()
  }

  render(): JSX.Element {
    if (this.state.contents.kind === ResultKind.Loading) {
      return (
        <Card size="small">
          <Skeleton />
        </Card>
      )
    }
    if (this.state.contents.kind === ResultKind.Failure) {
      return (
        <Card size="small">
          <p>{this.state.contents.message}</p>
          <Button onClick={this.getContents.bind(this, true)}>重试</Button>
        </Card>
      )
    }

    const { questions } = this.state.contents.payload

    return (
      <Card
        size="small"
        title={
          <Pagination
            current={this.state.questionIndex}
            total={questions.length}
            pageSize={1}
            showSizeChanger={false}
            onChange={this.handleQuestionChange.bind(this)}
          />
        }
        activeTabKey={this.state.tabKey}
        onTabChange={this.handleTabChange.bind(this)}
        tabProps={{ size: 'small' }}
        tabList={[
          { key: TabKey.Summary, tab: '题目' },
          { key: TabKey.Answer, tab: '答案' },
          { key: TabKey.Explanation, tab: '解析' }
        ]}
      >
        {this.getTabContents()}
      </Card>
    )
  }

  private getTabContents(): JSX.Element {
    const { contents } = this.state
    if (contents.kind === ResultKind.Loading) {
      return <Skeleton />
    }
    if (contents.kind === ResultKind.Failure) {
      return (
        <>
          <p>{contents.message}</p>
          <Button onClick={this.getContents.bind(this, true)}>重试</Button>
        </>
      )
    }

    const question = contents.payload.questions[this.state.questionIndex - 1]
    return (
      <div
        className="question"
        ref={(div) => {
          if (!div) return
          while (div.hasChildNodes()) {
            div.removeChild(div.lastChild!)
          }
          div.appendChild(question[this.state.tabKey])
        }}
      />
    )
  }

  private getContents(showLoading = false) {
    if (showLoading) {
      this.setState({ contents: loadingResult })
    }
    getExerciseContents(this.props.id).then((contents) => {
      if (contents.kind === ResultKind.Success) {
        this.props.nav.setMetadata({
          title: contents.payload.title,
          canBack: true
        })
        message.success(`加载成功：共 ${contents.payload.questions.length} 题`)
      } else {
        this.props.nav.setMetadata({
          title: '练习加载失败',
          canBack: true
        })
        message.error(contents.message)
      }
      this.setState({ contents })
    })
  }

  private handleQuestionChange(questionIndex: number) {
    this.setState({ questionIndex })
  }

  private handleTabChange(tabKey: string) {
    this.setState({ tabKey: tabKey as TabKey })
  }
}
