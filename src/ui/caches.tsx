import { Button, Card, message } from 'antd'
import * as React from 'react'
import { ResultKind } from '../model/model-general'
import { NavController } from '../model/nav'
import { clearCache } from '../model/sw-controller'

interface CachesProps {
  nav: NavController
}

interface CachesState {}

export class CachesPage extends React.Component<CachesProps, CachesState> {
  constructor(props: CachesProps) {
    super(props)
    this.state = {}
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '缓存管理', canBack: true })
  }

  render(): JSX.Element {
    return (
      <Card size="small">
        <Button onClick={this.handleClearCache}>清空</Button>
      </Card>
    )
  }

  private async handleClearCache() {
    const result = await clearCache()
    if (result.kind === ResultKind.Failure) {
      message.error(`缓存清空失败：${result.message}`)
    } else {
      message.success('缓存清空成功')
    }
  }
}
