import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Card, Checkbox, Form, Input, message } from 'antd'
import { Store } from 'antd/lib/form/interface'
import * as React from 'react'
import { ResultKind } from '../model/model-general'
import { NavController } from '../model/nav'
import { getUser, saveUser } from '../model/user'

interface LoginProps {
  nav: NavController
}

export class LoginPage extends React.Component<LoginProps> {
  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '登录', canBack: false })
  }

  render(): JSX.Element {
    return (
      <Card>
        <Form
          initialValues={{ persistent: true }}
          onFinish={this.handleLogin.bind(this)}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: '请输入用户名（身份证号）' }]}
          >
            <Input
              prefix={<UserOutlined />}
              autoComplete="username"
              placeholder="用户名"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: '请输入密码（默认“666666”）' }]}
          >
            <Input.Password
              prefix={<LockOutlined />}
              autoComplete="current-password"
              placeholder="密码"
            />
          </Form.Item>
          <Form.Item name="persistent" valuePropName="checked">
            <Checkbox>自动登录</Checkbox>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              登录
            </Button>
          </Form.Item>
        </Form>
      </Card>
    )
  }

  private async handleLogin(values: Store): Promise<void> {
    const user = await getUser(values.username, values.password)
    if (user.kind === ResultKind.Failure) {
      message.error(user.message)
      return
    }
    message.success(`登录成功：${user.payload.name}`)
    saveUser(user.payload, values.persistent)
    this.props.nav.navigateTo('')
  }
}
