import { MenuOutlined } from '@ant-design/icons'
import { Button, Card, Dropdown, Menu, message, PageHeader } from 'antd'
import * as React from 'react'
import { NavController, PageMetadata } from '../model/nav'
import { clearUser, readUser } from '../model/user'
import { CachesPage } from './caches'
import { ExerciseListPage } from './exercise-list'
import { ExerciseViewerPage } from './exercise-viewer'
import { HomePage } from './home'
import { LoginPage } from './login'
import { MaterialListPage } from './material-list'
import { MaterialViewerPage } from './material-viewer'
import { SettingsPage } from './settings'

interface AppProps {}

interface AppState {
  metadata: PageMetadata
}

export class App extends React.Component<AppProps, AppState> {
  private readonly nav = new NavController(
    () => this.setState({}),
    (metadata) => this.setState({ metadata })
  )

  constructor(props: AppProps) {
    super(props)
    this.state = {
      metadata: {
        title: document.title,
        canBack: false
      }
    }
  }

  render(): JSX.Element {
    return (
      <>
        <PageHeader
          title={this.state.metadata.title}
          onBack={
            this.state.metadata.canBack
              ? this.nav.navigateTo.bind(this.nav, '')
              : undefined
          }
          extra={
            <Dropdown overlay={this.getMenu()}>
              <Button shape="circle" icon={<MenuOutlined />} />
            </Dropdown>
          }
        />
        {this.renderContent()}
      </>
    )
  }

  private renderContent(): JSX.Element {
    const url = new URL(location.href)
    const user = readUser()

    if (url.searchParams.has('exercises')) {
      if (!user) {
        message.error('请登录以查看练习')
        this.nav.navigateTo('')
        return (
          <Card size="small">
            <p>正在转到登录界面</p>
          </Card>
        )
      }
      return <ExerciseListPage user={user} nav={this.nav} />
    }

    const exerciseId = url.searchParams.get('exercise')
    if (exerciseId) {
      return <ExerciseViewerPage id={exerciseId} nav={this.nav} />
    }

    if (url.searchParams.has('materials')) {
      if (!user) {
        message.error('请登录以查看导学案')
        this.nav.navigateTo('')
        return (
          <Card size="small">
            <p>正在转到登录界面</p>
          </Card>
        )
      }
      return <MaterialListPage user={user} nav={this.nav} />
    }

    const materialId = url.searchParams.get('material')
    if (materialId) {
      return <MaterialViewerPage id={materialId} nav={this.nav} />
    }

    if (url.searchParams.has('caches')) {
      return <CachesPage nav={this.nav} />
    }

    if (url.searchParams.has('settings')) {
      return <SettingsPage nav={this.nav} />
    }

    if (user) {
      return <HomePage nav={this.nav} />
    }
    return <LoginPage nav={this.nav} />
  }

  private getMenu(): JSX.Element {
    const user = readUser()
    if (!user) {
      return (
        <Menu onClick={this.handleMenuClick.bind(this)}>
          <Menu.Item key="login">登录</Menu.Item>
          <Menu.Item key="settings">设置</Menu.Item>
          <Menu.Item key="caches">缓存管理</Menu.Item>
        </Menu>
      )
    }
    return (
      <Menu onClick={this.handleMenuClick.bind(this)}>
        <Menu.Item disabled>用户：{user.name}</Menu.Item>
        <Menu.Item key="settings">设置</Menu.Item>
        <Menu.Item key="caches">缓存管理</Menu.Item>
        <Menu.Item key="logout">退出登录</Menu.Item>
      </Menu>
    )
  }

  private handleMenuClick({ key }: { key: React.ReactText }): void {
    switch (key) {
      case 'caches': {
        this.nav.navigateTo('?caches')
        break
      }
      case 'settings': {
        this.nav.navigateTo('?settings')
        break
      }
      case 'login': {
        this.nav.navigateTo('')
        break
      }
      case 'logout': {
        clearUser()
        this.nav.navigateTo('')
      }
    }
  }
}
