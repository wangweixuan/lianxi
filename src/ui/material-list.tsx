import { Button, Card, List, message, Skeleton } from 'antd'
import * as React from 'react'
import { getMaterialList } from '../model/loader-material'
import { LoadedResult, loadingResult, ResultKind } from '../model/model-general'
import { MaterialList } from '../model/model-material'
import { NavController } from '../model/nav'
import { User } from '../model/user'
import { ItemComponent } from './item'

interface MaterialListProps {
  user: User
  nav: NavController
}

interface MaterialListState {
  materials: LoadedResult<MaterialList>
  page: number
  pageSize: number
}

export class MaterialListPage extends React.Component<
  MaterialListProps,
  MaterialListState
> {
  constructor(props: MaterialListProps) {
    super(props)

    this.state = {
      materials: loadingResult,
      page: 1,
      pageSize: 10
    }
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '导学案列表', canBack: true })
    this.getMaterials()
  }

  render(): JSX.Element {
    if (this.state.materials.kind === ResultKind.Loading) {
      return (
        <Card size="small">
          <Skeleton />
        </Card>
      )
    }
    if (this.state.materials.kind === ResultKind.Failure) {
      return (
        <Card size="small">
          <p>{this.state.materials.message}</p>
          <Button onClick={this.getMaterials.bind(this, true)}>重试</Button>
        </Card>
      )
    }

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this
    return (
      <Card size="small">
        <List
          dataSource={this.state.materials.payload.materials}
          renderItem={(material) => (
            <ItemComponent content={material} nav={this.props.nav} />
          )}
          locale={{ emptyText: '啊哈哈哈 (ಡωಡ) hiahiahia' }}
          pagination={{
            total:
              this.state.materials.payload.totalPages * this.state.pageSize,
            current: this.state.page,
            pageSize: this.state.pageSize,
            showSizeChanger: false,
            onChange(page) {
              _this.setState({ page }, _this.getMaterials.bind(_this, false))
            }
          }}
        />
      </Card>
    )
  }

  private getMaterials(showLoading = false) {
    if (showLoading) {
      this.setState({ materials: loadingResult })
    }
    getMaterialList(this.props.user, this.state.page, this.state.pageSize).then(
      (materials) => {
        if (materials.kind === ResultKind.Success) {
          message.success(`加载成功：共 ${materials.payload.totalPages} 页`)
        } else if (materials.kind === ResultKind.Failure) {
          message.error(materials.message)
        }
        this.setState({ materials })
      }
    )
  }
}
