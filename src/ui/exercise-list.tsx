import { Button, Card, List, message, Skeleton } from 'antd'
import * as React from 'react'
import { getExerciseList } from '../model/loader-exercise'
import { ExerciseFilter, ExerciseList } from '../model/model-exercise'
import { LoadedResult, loadingResult, ResultKind } from '../model/model-general'
import { NavController } from '../model/nav'
import { User } from '../model/user'
import { FilterComponent } from './exercise-filter'
import { ItemComponent } from './item'

interface ExerciseListProps {
  user: User
  nav: NavController
}

interface ExerciseListState {
  exercises: LoadedResult<ExerciseList>
  filter: ExerciseFilter
  page: number
  pageSize: number
}

export class ExerciseListPage extends React.Component<
  ExerciseListProps,
  ExerciseListState
> {
  constructor(props: ExerciseListProps) {
    super(props)

    this.state = {
      exercises: loadingResult,
      filter: ExerciseFilter.emptyFilter,
      page: 1,
      pageSize: 10
    }
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '练习列表', canBack: true })
  }

  render(): JSX.Element {
    return (
      <>
        <FilterComponent
          user={this.props.user}
          onFilter={(filter) =>
            this.setState(
              { filter, page: 1 },
              this.getExercises.bind(this, true)
            )
          }
        />
        {this.renderList()}
      </>
    )
  }

  private renderList(): JSX.Element {
    if (this.state.exercises.kind === ResultKind.Loading) {
      return (
        <Card size="small">
          <Skeleton />
        </Card>
      )
    }
    if (this.state.exercises.kind === ResultKind.Failure) {
      return (
        <Card size="small">
          <p>{this.state.exercises.message}</p>
          <Button onClick={this.getExercises.bind(this, true)}>重试</Button>
        </Card>
      )
    }

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this
    return (
      <Card size="small">
        <List
          dataSource={this.state.exercises.payload.exercises}
          renderItem={(exercise) => (
            <ItemComponent content={exercise} nav={this.props.nav} />
          )}
          locale={{ emptyText: '啊哈哈哈 (ಡωಡ) hiahiahia' }}
          pagination={{
            total: this.state.exercises.payload.total,
            current: this.state.page,
            pageSize: this.state.pageSize,
            showSizeChanger: false,
            onChange(page) {
              _this.setState({ page }, _this.getExercises.bind(_this, false))
            }
          }}
        />
      </Card>
    )
  }

  private getExercises(showLoading = false) {
    if (showLoading) {
      this.setState({ exercises: loadingResult })
    }
    getExerciseList(
      this.props.user,
      this.state.filter,
      this.state.page,
      this.state.pageSize
    ).then((exercises) => {
      if (exercises.kind === ResultKind.Success) {
        message.success(`加载成功：共 ${exercises.payload.total} 份`)
      } else if (exercises.kind === ResultKind.Failure) {
        message.error(exercises.message)
      }
      this.setState({ exercises })
    })
  }
}
