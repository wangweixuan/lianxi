import { Button, Card, message, Pagination, Skeleton } from 'antd'
import * as React from 'react'
import { getMaterialContents } from '../model/loader-material'
import { LoadedResult, loadingResult, ResultKind } from '../model/model-general'
import { MaterialContents } from '../model/model-material'
import { BASE_URL, NavController } from '../model/nav'

interface MaterialViewerProps {
  id: string
  nav: NavController
}

interface MaterialViewerState {
  contents: LoadedResult<MaterialContents>
  page: number
}

export class MaterialViewerPage extends React.Component<
  MaterialViewerProps,
  MaterialViewerState
> {
  constructor(props: MaterialViewerProps) {
    super(props)
    this.state = { contents: loadingResult, page: 1 }
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '导学案加载中', canBack: true })
    this.getContents()
  }

  render(): JSX.Element {
    if (this.state.contents.kind === ResultKind.Loading) {
      return (
        <Card size="small">
          <Skeleton />
        </Card>
      )
    }
    if (this.state.contents.kind === ResultKind.Failure) {
      return (
        <Card size="small">
          <p>{this.state.contents.message}</p>
          <Button onClick={this.getContents.bind(this)}>重试</Button>
        </Card>
      )
    }

    return (
      <Card
        size="small"
        className="question"
        title={
          <Pagination
            current={this.state.page}
            total={this.state.contents.payload.totalPages}
            pageSize={1}
            showSizeChanger={false}
            onChange={this.handlePage.bind(this)}
          />
        }
      >
        <img
          src={
            BASE_URL +
            `/sw/material-resource?id=${this.props.id}&page=${
              this.state.page - 1
            }`
          }
        />
      </Card>
    )
  }

  private getContents() {
    this.setState({ contents: loadingResult })
    getMaterialContents(this.props.id).then((contents) => {
      if (contents.kind === ResultKind.Success) {
        this.props.nav.setMetadata({ title: '导学案', canBack: true })
        message.success(`加载成功：共 ${contents.payload.totalPages} 页`)
      } else {
        this.props.nav.setMetadata({
          title: '导学案加载失败',
          canBack: true
        })
        message.error(contents.message)
      }
      this.setState({ contents })
    })
  }

  private handlePage(page: number) {
    this.setState({ page })
  }
}
