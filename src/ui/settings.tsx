import { Button, Card, Form, message, Switch } from 'antd'
import * as React from 'react'
import { registerWorker, unregisterWorker } from '../model/sw-controller'
import { NavController } from '../model/nav'
import { readDarkMode, saveDarkMode } from '../model/settings'

interface SettingsProps {
  nav: NavController
}

interface SettingsState {
  darkMode: boolean
}

export class SettingsPage extends React.Component<
  SettingsProps,
  SettingsState
> {
  constructor(props: SettingsProps) {
    super(props)
    this.state = { darkMode: readDarkMode() }
  }

  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '设置', canBack: true })
  }

  render(): JSX.Element {
    return (
      <Card size="small">
        <Form>
          <Form.Item label="暗色模式">
            <Switch
              defaultChecked={this.state.darkMode}
              onChange={this.handleDarkMode.bind(this)}
            />
          </Form.Item>
          <Form.Item label="ServiceWorker 控制">
            <Button onClick={registerWorker}>安装</Button>
            <Button onClick={unregisterWorker}>卸载</Button>
          </Form.Item>
        </Form>
      </Card>
    )
  }

  private handleDarkMode(value: boolean) {
    saveDarkMode(value)
    this.setState({ darkMode: value })
    message.success('暗色模式已切换')
  }
}
