import { Card, Col, Row } from 'antd'
import * as React from 'react'
import { NavController } from '../model/nav'

interface HomeProps {
  nav: NavController
}

export class HomePage extends React.Component<HomeProps> {
  componentDidMount(): void {
    this.props.nav.setMetadata({ title: '学习本助手', canBack: false })
  }

  render(): JSX.Element {
    return (
      <Row>
        <Col span={12}>
          <Card
            cover={<img src="./assets/exercise.svg" alt="练习" />}
            hoverable
            onClick={this.handleNavigateToExercises.bind(this)}
          >
            <Card.Meta title="练习" />
          </Card>
        </Col>
        <Col span={12}>
          <Card
            cover={<img src="./assets/material.svg" alt="导学案" />}
            hoverable
            onClick={this.handleNavigateToMaterials.bind(this)}
          >
            <Card.Meta title="导学案" />
          </Card>
        </Col>
      </Row>
    )
  }

  private handleNavigateToExercises() {
    this.props.nav.navigateTo('?exercises')
  }

  private handleNavigateToMaterials() {
    this.props.nav.navigateTo('?materials')
  }
}
