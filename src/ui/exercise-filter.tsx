import { Button, Card, Form, Input, Radio, Select, Skeleton } from 'antd'
import { RadioChangeEvent } from 'antd/lib/radio'
import { SelectValue } from 'antd/lib/select'
import * as React from 'react'
import { getSubjects } from '../model/loader-exercise'
import { ExerciseFilter } from '../model/model-exercise'
import { LoadedResult, loadingResult, ResultKind } from '../model/model-general'
import { User } from '../model/user'

interface FilterProps {
  user: User
  onFilter: (filter: ExerciseFilter) => void
}

interface FilterState {
  subjects: LoadedResult<ExerciseFilter.Subject[]>
  filter: ExerciseFilter
}

export class FilterComponent extends React.Component<FilterProps, FilterState> {
  constructor(props: FilterProps) {
    super(props)
    this.state = {
      subjects: loadingResult,
      filter: ExerciseFilter.emptyFilter
    }
  }

  componentDidMount(): void {
    this.getSubjects()
    this.onFilter()
  }

  render(): JSX.Element {
    if (this.state.subjects.kind === ResultKind.Loading) {
      return (
        <Card size="small">
          <Skeleton />
        </Card>
      )
    }
    if (this.state.subjects.kind === ResultKind.Failure) {
      return (
        <Card size="small">
          <p>{this.state.subjects.message}</p>
          <Button onClick={this.getSubjects.bind(this, true)}>重试</Button>
        </Card>
      )
    }

    return (
      <Card size="small">
        <Form
          initialValues={{
            search: this.state.filter.search,
            subject: this.state.filter.subject.value,
            date: this.state.filter.date.value,
            stage: this.state.filter.stage.value
          }}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item label="搜索" name="search">
            <Input.Search
              onChange={this.handleSearch.bind(this)}
              onSearch={this.onFilter.bind(this)}
              placeholder="作业或试题"
            />
          </Form.Item>
          <Form.Item label="学科" name="subject">
            <Select
              options={this.state.subjects.payload}
              onChange={this.handleSubject.bind(this)}
            />
          </Form.Item>
          <Form.Item label="日期" name="date">
            <Select
              options={ExerciseFilter.Date.VALUES}
              onChange={this.handleDate.bind(this)}
            />
          </Form.Item>
          <Form.Item label="状态" name="stage">
            <Radio.Group
              options={ExerciseFilter.Stage.VALUES}
              optionType="button"
              buttonStyle="solid"
              onChange={this.handleStage.bind(this)}
            />
          </Form.Item>
          <Form.Item wrapperCol={{ sm: { offset: 4, span: 20 } }}>
            <Button onClick={this.onFilter.bind(this)}>刷新</Button>
          </Form.Item>
        </Form>
      </Card>
    )
  }

  private getSubjects(showLoading = false) {
    if (showLoading) {
      this.setState({ subjects: loadingResult })
    }
    getSubjects(this.props.user).then((subjects) => {
      this.setState({ subjects })
    })
  }

  private handleSearch(e: React.ChangeEvent<HTMLInputElement>) {
    const value = e.target.value
    this.setState((prev) => ({
      filter: { ...prev.filter, search: value }
    }))
  }

  private handleSubject(value: SelectValue) {
    this.setState((prev) => {
      if (prev.subjects.kind !== ResultKind.Success) {
        throw Error()
      }
      const filter: ExerciseFilter = {
        ...prev.filter,
        subject: prev.subjects.payload.find((it) => it.value === value)!
      }
      this.props.onFilter(filter)
      return { filter }
    })
  }

  private handleDate(value: SelectValue) {
    this.setState((prev) => {
      const filter: ExerciseFilter = {
        ...prev.filter,
        date: ExerciseFilter.Date.VALUES.find((it) => it.value === value)!
      }
      this.props.onFilter(filter)
      return { filter }
    })
  }

  private handleStage(e: RadioChangeEvent) {
    const value = e.target.value
    this.setState((prev) => {
      const filter: ExerciseFilter = {
        ...prev.filter,
        stage: ExerciseFilter.Stage.VALUES.find((it) => it.value === value)!
      }
      this.props.onFilter(filter)
      return { filter }
    })
  }

  private onFilter() {
    this.props.onFilter(this.state.filter)
  }
}
