import { Avatar, List } from 'antd'
import * as React from 'react'
import { Exercise } from '../model/model-exercise'
import { Material } from '../model/model-material'
import { NavController } from '../model/nav'
import { getAvatarColor } from '../model/utilities'

export function ItemComponent(props: {
  content: Exercise | Material
  nav: NavController
}): JSX.Element {
  const type = props.content instanceof Exercise ? 'exercise' : 'material'
  const navTarget = `?${type}=${props.content.id}`
  const handler = (event: React.MouseEvent) => {
    if (event.altKey || event.ctrlKey || event.metaKey || event.shiftKey) return
    event.preventDefault()
    props.nav.navigateTo(navTarget)
  }

  return (
    <List.Item onClick={handler}>
      <List.Item.Meta
        avatar={
          <Avatar
            style={{
              backgroundColor: getAvatarColor(props.content.subject)
            }}
          >
            {props.content.subject.charAt(0)}
          </Avatar>
        }
        title={
          <a href={navTarget} onClick={handler}>
            {props.content.title}
          </a>
        }
        description={props.content.toFriendlyString()}
      />
    </List.Item>
  )
}
