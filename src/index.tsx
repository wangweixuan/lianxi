import { message } from 'antd'
import * as React from 'react'
import { render } from 'react-dom'
import { readDarkMode, refreshDarkMode } from './model/settings'
import {
  clearCache,
  isWorkerSupported,
  registerWorker
} from './model/sw-controller'
import { App } from './ui/app'

render(<App />, document.getElementById('app'))

if (readDarkMode()) {
  refreshDarkMode(true)
}

if (isWorkerSupported()) {
  window.addEventListener('load', registerWorker)
} else {
  message.warn('ServiceWorker 不受支持')
}

if (localStorage.getItem('updated') !== '1') {
  localStorage.setItem('updated', '1')
  clearCache()
}
