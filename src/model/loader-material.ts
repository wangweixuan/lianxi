import { parseJson } from './json-bigint'
import {
  FailableResult,
  ResultKind,
  toFailureResult,
  toSuccessResult
} from './model-general'
import { Material, MaterialContents, MaterialList } from './model-material'
import { BASE_URL } from './nav'
import { handleError, handleFetch } from './network'
import { isWorkerActive } from './sw-controller'
import { User } from './user'

const API_MATERIAL_LIST = 'https://edu.learnonly.com/api/v1/dx/studentDxList'

export async function getMaterialList(
  user: User,
  page: number,
  pageSize: number
): Promise<FailableResult<MaterialList>> {
  try {
    const response = await handleFetch(API_MATERIAL_LIST, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `order=0&state=10&time=0&studentid=${user.uid}\
&page=${page}&pagesize=${pageSize}`
    })
    if (response.kind === ResultKind.Failure) {
      return response
    }

    const json = parseJson(await response.payload.text()) as any
    return toSuccessResult(
      new MaterialList(
        (json.data.list as any[]).map(
          (item) =>
            new Material(
              item.dxId,
              item.title,
              item.subjectName,
              new Date(item.startTime)
            )
        ),
        Number(json.data.totalpage)
      )
    )
  } catch (err: unknown) {
    return handleError(err)
  }
}

export async function getMaterialContents(
  id: string
): Promise<FailableResult<MaterialContents>> {
  if (!isWorkerActive()) {
    return toFailureResult('ServiceWorker 未激活')
  }

  try {
    const response = await handleFetch(BASE_URL + '/sw/material?id=' + id)
    if (response.kind === ResultKind.Failure) {
      return response
    }

    const totalPages = Number(await response.payload.text())

    return toSuccessResult(new MaterialContents(totalPages))
  } catch (err: unknown) {
    return handleError(err)
  }
}
