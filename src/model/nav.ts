export const BASE_URL = process.env.LIANXI_BASE_URL ?? ''

type NavListener = () => void

type MetadataListener = (metadata: PageMetadata) => void

export interface PageMetadata {
  title: string
  canBack: boolean
}

export class NavController {
  constructor(
    private readonly onNavigate: NavListener,
    private readonly onMetadata: MetadataListener
  ) {
    window.addEventListener('popstate', onNavigate)
  }

  navigateTo(path: string): void {
    const url = new URL(location.href)
    url.search = path
    history.pushState(null, document.title, url.href)
    this.onNavigate()
  }

  setMetadata(metadata: PageMetadata): void {
    this.onMetadata(metadata)
  }
}
