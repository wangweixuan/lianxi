import { FailableResult, toFailureResult, toSuccessResult } from "./model-general"

export async function handleFetch(
  request: RequestInfo,
  requestInit?: RequestInit
): Promise<FailableResult<Response>> {
  try {
    const response = await fetch(request, requestInit)
    if (!response.ok) {
      console.error(response)
      return toFailureResult('加载失败：服务器错误')
    }
    return toSuccessResult(response)
  } catch (err: unknown) {
    console.error(err)
    return toFailureResult('加载失败：网络错误')
  }
}

export function handleError(err: unknown): FailableResult<never> {
  console.error(err)
  if (err instanceof SyntaxError || err instanceof TypeError) {
    return toFailureResult('加载失败：数据格式错误')
  }
  return toFailureResult('加载失败：未知错误')
}
