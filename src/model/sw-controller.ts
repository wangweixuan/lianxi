import { message } from 'antd'
import {
  FailableResult,
  toFailureResult,
  toSuccessResult
} from './model-general'
import { BASE_URL } from './nav'
import { handleError } from './network'

export function isWorkerSupported(): boolean {
  return 'serviceWorker' in navigator
}

export function isWorkerActive(): boolean {
  return 'serviceWorker' in navigator && !!navigator.serviceWorker.controller
}

navigator.serviceWorker?.addEventListener('controllerchange', () => {
  if (navigator.serviceWorker.controller) {
    message.info('ServiceWorker 已激活')
  } else {
    message.info('ServiceWorker 已取消激活')
  }
})

export function registerWorker(): void {
  navigator.serviceWorker.register(BASE_URL + '/sw.js').then(
    () => {
      message.info('ServiceWorker 安装成功')
    },
    (reason) => {
      message.error('ServiceWorker 安装失败')
      console.log(reason)
    }
  )
}

export function unregisterWorker(): void {
  navigator.serviceWorker
    .getRegistration(BASE_URL + '/sw.js')
    .then((registration) => {
      if (!registration) {
        message.info('ServiceWorker 未安装')
        return
      }
      registration.unregister().then(
        () => {
          message.info('ServiceWorker 卸载成功')
          location.reload()
        },
        (reason) => {
          message.error('ServiceWorker 卸载失败')
          console.log(reason)
        }
      )
    })
}

export async function clearCache(): Promise<FailableResult<void>> {
  if (!isWorkerActive()) {
    return toFailureResult('ServiceWorker 未激活')
  }

  try {
    await fetch(BASE_URL + '/sw/clear-caches', { method: 'POST' })
    return toSuccessResult(undefined)
  } catch (err: unknown) {
    return handleError(err)
  }
}
