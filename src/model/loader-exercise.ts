import { parseJson } from './json-bigint'
import {
  Exercise,
  ExerciseContents,
  ExerciseFilter,
  ExerciseList,
  Question
} from './model-exercise'
import {
  FailableResult,
  ResultKind,
  toFailureResult,
  toSuccessResult
} from './model-general'
import { BASE_URL } from './nav'
import { handleError, handleFetch } from './network'
import { isWorkerActive } from './sw-controller'
import { User } from './user'

const API_EXERCISE_BASE = 'https://edu.celianyi.com/api/exercise/app'

export async function getSubjects(
  user: User
): Promise<FailableResult<ExerciseFilter.Subject[]>> {
  try {
    const response = await handleFetch(
      API_EXERCISE_BASE + '/banks?userId=' + user.uid
    )
    if (response.kind === ResultKind.Failure) {
      return response
    }

    const json = parseJson(await response.payload.text()) as any
    return toSuccessResult([
      ExerciseFilter.Subject.ALL,
      ...(json.bankList as any[]).map(
        (item) => new ExerciseFilter.Subject(item.id, item.name)
      )
    ])
  } catch (err: unknown) {
    return handleError(err)
  }
}

export async function getExerciseList(
  user: User,
  filter: ExerciseFilter,
  page: number,
  pageSize: number
): Promise<FailableResult<ExerciseList>> {
  try {
    const response = await handleFetch(
      `${API_EXERCISE_BASE}/exerciseList\
?userId=${user.uid}&keyword=${encodeURIComponent(filter.search)}\
&bankId=${filter.subject.value}&time=${filter.date.value}
&typeId=${filter.stage.value}&page=${page}&pageSize=${pageSize}`
    )
    if (response.kind === ResultKind.Failure) {
      return response
    }

    const json = parseJson(await response.payload.text()) as any
    return toSuccessResult(
      new ExerciseList(
        (json.exerciseList as any[]).map(
          (item) =>
            new Exercise(
              item.exerciseId,
              item.title,
              item.subjectName,
              new Date(item.pushDate),
              Number(item.objectiveCount),
              Number(item.questionCount) - Number(item.objectiveCount)
            )
        ),
        Number(json.countExercises)
      )
    )
  } catch (err: unknown) {
    return handleError(err)
  }
}

export async function getExerciseContents(
  id: string
): Promise<FailableResult<ExerciseContents>> {
  if (!isWorkerActive()) {
    return toFailureResult('ServiceWorker 未激活')
  }

  try {
    const response = await handleFetch(BASE_URL + '/sw/exercise?id=' + id)
    if (response.kind === ResultKind.Failure) {
      return response
    }

    const text = await response.payload.text()
    const doc = new DOMParser().parseFromString(text, 'text/html')
    // const doc = sanitize(text, { RETURN_DOM: true, WHOLE_DOCUMENT: true })

    const summaries = doc.getElementsByClassName('content-box')
    const answers = doc.getElementsByClassName('answerAnalysis')

    const questions = Array.from(summaries, (summary, index) => {
      const answer = answers[index]
      sanitizeLabels(answer, 1)
      const explanation = answer.cloneNode(true) as Element
      sanitizeLabels(answer, -1)
      sanitizeLabels(explanation, 1)
      sanitizeImages(summary, id)
      sanitizeImages(answer, id)
      sanitizeImages(explanation, id)
      return new Question(summary, answer, explanation)
    })

    return toSuccessResult(new ExerciseContents(doc.title, questions))
  } catch (err: unknown) {
    return handleError(err)
  }
}

function sanitizeLabels(element: Element, direction: 1 | -1) {
  while (element.hasChildNodes()) {
    const removed = element.removeChild(
      direction > 0 ? element.firstChild! : element.lastChild!
    )
    if (removed instanceof Element && removed.className === 'ae') {
      break
    }
  }
}

function sanitizeImages(element: Element, id: string) {
  const images = Array.from(element.getElementsByTagName('img'))
  for (const image of images) {
    const src = image.getAttribute('src')
    if (src?.startsWith('./')) {
      image.setAttribute(
        'src',
        `${BASE_URL}/sw/exercise-resource?id=${id}&filename=${src.substring(2)}`
      )
    }
  }
}
