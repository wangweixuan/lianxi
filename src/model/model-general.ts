export const enum ResultKind {
  Success,
  Loading,
  Failure
}

export type FailableResult<T> =
  | {
      kind: ResultKind.Success
      payload: T
    }
  | {
      kind: ResultKind.Failure
      message: string
    }

export type LoadedResult<T> =
  | FailableResult<T>
  | {
      kind: ResultKind.Loading
    }

export const loadingResult: LoadedResult<never> = { kind: ResultKind.Loading }

export function toSuccessResult<T>(payload: T): FailableResult<T> {
  return {
    kind: ResultKind.Success,
    payload
  }
}

export function toFailureResult(message: string): FailableResult<never> {
  return {
    kind: ResultKind.Failure,
    message
  }
}
