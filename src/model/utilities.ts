const AVATAR_COLORS = [
  '#f5222d',
  '#fa541c',
  '#d48806',
  '#389e0d',
  '#08979c',
  '#1890ff',
  '#722ed1',
  '#eb2f96',
  '#8c8c8c'
]

shuffle(AVATAR_COLORS)

const knownIds: unknown[] = []

export function getAvatarColor(id: unknown): string {
  let index = knownIds.indexOf(id)
  if (index === -1) {
    index = knownIds.push(id) - 1
  }
  return AVATAR_COLORS[index % AVATAR_COLORS.length]
}

function shuffle(array: unknown[]) {
  let i = array.length
  while (i) {
    const j = Math.floor(Math.random() * i--)
    ;[array[j], array[i]] = [array[i], array[j]]
  }
}
