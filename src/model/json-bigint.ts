// Adapted from https://lihautan.com/json-parser-with-javascript/

export function parseJson(str: string): unknown {
  let i = 0

  const value = parseValue()
  if (i < str.length) {
    throw new SyntaxError()
  }
  return value

  function parseObject() {
    if (str[i] !== '{') return
    i++
    skipWhitespace()

    const result = {}

    let initial = true
    // if it is not '}',
    // we take the path of string -> whitespace -> ':' -> value -> ...
    while (i < str.length && str[i] !== '}') {
      if (!initial) {
        eatComma()
        skipWhitespace()
      }
      const key = parseString()
      if (key === undefined) {
        throw new SyntaxError()
      }
      skipWhitespace()
      eatColon()
      const value = parseValue()
      Object.defineProperty(result, key, { value })
      initial = false
    }
    if (i === str.length) {
      throw new SyntaxError()
    }
    // move to the next character of '}'
    i++

    return result
  }

  function parseArray(): unknown[] | undefined {
    if (str[i] !== '[') return
    i++
    skipWhitespace()

    const result = []
    let initial = true
    while (i < str.length && str[i] !== ']') {
      if (!initial) {
        eatComma()
      }
      const value = parseValue()
      result.push(value)
      initial = false
    }
    if (i === str.length) {
      throw new SyntaxError()
    }
    // move to the next character of ']'
    i++
    return result
  }

  function parseValue(): unknown {
    skipWhitespace()
    const value =
      parseString() ??
      parseNumber() ??
      parseObject() ??
      parseArray() ??
      parseKeyword('true', true) ??
      parseKeyword('false', false) ??
      parseKeyword('null', null)
    skipWhitespace()
    return value
  }

  function parseKeyword(name: string, value: unknown): unknown {
    if (str.slice(i, i + name.length) !== name) return
    i += name.length
    return value
  }

  function skipWhitespace(): void {
    while (
      str[i] === ' ' ||
      str[i] === '\n' ||
      str[i] === '\t' ||
      str[i] === '\r'
    ) {
      i++
    }
  }

  function parseString(): string | undefined {
    if (str[i] !== '"') return
    i++
    let result = ''
    while (i < str.length && str[i] !== '"') {
      if (str[i] === '\\') {
        const char = str[i + 1]
        if (
          char === '"' ||
          char === '\\' ||
          char === '/' ||
          char === 'b' ||
          char === 'f' ||
          char === 'n' ||
          char === 'r' ||
          char === 't'
        ) {
          result += char
          i++
        } else if (char === 'u') {
          if (
            isHexadecimal(str[i + 2]) &&
            isHexadecimal(str[i + 3]) &&
            isHexadecimal(str[i + 4]) &&
            isHexadecimal(str[i + 5])
          ) {
            result += String.fromCharCode(parseInt(str.slice(i + 2, i + 6), 16))
            i += 5
          } else {
            i += 2
            throw new SyntaxError()
          }
        } else {
          throw new SyntaxError()
        }
      } else {
        result += str[i]
      }
      i++
    }
    if (i === str.length) {
      throw new SyntaxError()
    }
    i++
    return result
  }

  function isHexadecimal(char: string): boolean {
    return (
      (char >= '0' && char <= '9') ||
      (char.toLowerCase() >= 'a' && char.toLowerCase() <= 'f')
    )
  }

  function parseNumber(): string | undefined {
    const start = i
    if (str[i] === '-') {
      i++
      if (!(str[i] >= '0' && str[i] <= '9')) {
        throw new SyntaxError()
      }
    }
    if (str[i] === '0') {
      i++
    } else if (str[i] >= '1' && str[i] <= '9') {
      i++
      while (str[i] >= '0' && str[i] <= '9') {
        i++
      }
    }

    if (str[i] === '.') {
      i++
      if (!(str[i] >= '0' && str[i] <= '9')) {
        throw new SyntaxError()
      }
      while (str[i] >= '0' && str[i] <= '9') {
        i++
      }
    }
    if (str[i] === 'e' || str[i] === 'E') {
      i++
      if (str[i] === '-' || str[i] === '+') {
        i++
      }
      if (!(str[i] >= '0' && str[i] <= '9')) {
        throw new SyntaxError()
      }
      while (str[i] >= '0' && str[i] <= '9') {
        i++
      }
    }
    if (i > start) {
      // return Number(str.slice(start, i))
      return str.slice(start, i)
    }
  }

  function eatComma() {
    if (str[i] !== ',') {
      throw new SyntaxError()
    }
    i++
  }

  function eatColon() {
    if (str[i] !== ':') {
      throw new SyntaxError()
    }
    i++
  }
}
