export function readDarkMode(): boolean {
  return localStorage.getItem('dark') == 'true'
}

export function saveDarkMode(value: boolean): void {
  localStorage.setItem('dark', value.toString())
  refreshDarkMode(value)
}

export function refreshDarkMode(value: boolean): void {
  const styleElement = document.getElementById('antd-style') as HTMLLinkElement
  if (value) {
    document.body.classList.add('dark')
    styleElement.href = styleElement.href.replace('antd.', 'antd.dark.')
  } else {
    document.body.classList.remove('dark')
    styleElement.href = styleElement.href.replace('dark.', '')
  }
}
