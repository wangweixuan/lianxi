import { format } from 'timeago.js'

export class Material {
  constructor(
    readonly id: string,
    readonly title: string,
    readonly subject: string,
    readonly timePublished: Date
  ) {}

  toFriendlyString(): string {
    const description = format(this.timePublished, 'zh_CN') + '发布'
    return description
  }
}

export class MaterialList {
  constructor(readonly materials: Material[], readonly totalPages: number) {}
}

export class MaterialContents {
  constructor(readonly totalPages: number) {}
}
