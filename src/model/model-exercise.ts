import { format } from 'timeago.js'

export class Exercise {
  constructor(
    readonly id: string,
    readonly title: string,
    readonly subject: string,
    readonly timePublished: Date,
    readonly numberObjective: number,
    readonly numberSubjective: number
  ) {}

  toFriendlyString(): string {
    let description = format(this.timePublished, 'zh_CN') + '发布'
    if (this.numberObjective > 0) {
      description += `，${this.numberObjective} 个客观题`
    }
    if (this.numberSubjective > 0) {
      description += `，${this.numberSubjective} 个主观题`
    }
    return description
  }
}

export interface ExerciseFilter {
  search: string
  subject: ExerciseFilter.Subject
  date: ExerciseFilter.Date
  stage: ExerciseFilter.Stage
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace ExerciseFilter {
  export class Subject {
    constructor(readonly value: string, readonly label: string) {}

    static readonly ALL = new Subject('', '全部')
  }

  export class Date {
    private constructor(readonly value: string, readonly label: string) {}

    static readonly VALUES: Date[] = [
      new Date('0', '全部'),
      new Date('1', '今天'),
      new Date('2', '昨天'),
      new Date('3', '前天'),
      new Date('4', '本周'),
      new Date('5', '上周'),
      new Date('6', '本月'),
      new Date('7', '上月')
    ]
  }

  export class Stage {
    private constructor(readonly value: string, readonly label: string) {}

    static readonly VALUES: Stage[] = [
      new Stage('0', '新作业'),
      new Stage('1', '待批改'),
      new Stage('2', '需订正'),
      new Stage('3', '已完成')
    ]
  }

  export const emptyFilter: ExerciseFilter = {
    search: '',
    subject: ExerciseFilter.Subject.ALL,
    date: ExerciseFilter.Date.VALUES[0],
    stage: ExerciseFilter.Stage.VALUES[0]
  }
}

export class ExerciseList {
  constructor(readonly exercises: Exercise[], readonly total: number) {}
}

export class ExerciseContents {
  constructor(readonly title: string, readonly questions: Question[]) {}
}

export class Question {
  constructor(
    readonly summary: Element,
    readonly answer: Element,
    readonly explanation: Element
  ) {}
}
