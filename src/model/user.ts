import { parseJson } from './json-bigint'
import {
  FailableResult,
  ResultKind,
  toFailureResult,
  toSuccessResult
} from './model-general'
import { handleError, handleFetch } from './network'

const URL_LOGIN = 'https://edu.learnonly.com/api/v1/learnonly/user/login'

export class User {
  constructor(readonly uid: string, readonly name: string) {}
}

export async function getUser(
  username: string,
  password: string
): Promise<FailableResult<User>> {
  try {
    const response = await handleFetch(URL_LOGIN, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `loginId=${encodeURIComponent(username)}\
&loginPwd=${encodeURIComponent(password)}`
    })
    if (response.kind !== ResultKind.Success) {
      return response
    }

    const json = parseJson(await response.payload.text()) as any
    if (json.code !== '200') {
      return toFailureResult('用户名或密码错误')
    }
    return toSuccessResult(new User(json.account.id, json.account.fullName))
  } catch (err: unknown) {
    return handleError(err)
  }
}

export function readUser(): User | undefined {
  return readFromStorage(sessionStorage) ?? readFromStorage(localStorage)
}

function readFromStorage(storage: Storage): User | undefined {
  const uid = storage.getItem('uid')
  const name = storage.getItem('name')
  if (!uid || !name) {
    return undefined
  }
  return new User(uid, name)
}

export function saveUser(user: User, persistent: boolean): void {
  const storage = persistent ? localStorage : sessionStorage
  storage.setItem('uid', user.uid)
  storage.setItem('name', user.name)
}

export function clearUser(): void {
  localStorage.removeItem('uid')
  localStorage.removeItem('name')
  sessionStorage.removeItem('uid')
  sessionStorage.removeItem('name')
}
