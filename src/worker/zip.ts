export function createZipReader(reader: zip.Reader): Promise<zip.ZipReader> {
  return new Promise((resolve, reject) => {
    zip.createReader(reader, resolve, reject)
  })
}

export function getReaderEntries(reader: zip.ZipReader): Promise<zip.Entry[]> {
  return new Promise((resolve, _reject) => {
    reader.getEntries(resolve)
  })
}

export function getEntryBlob(entry: zip.Entry, type: string): Promise<Blob> {
  return new Promise((resolve, _reject) => {
    entry.getData(new zip.BlobWriter(type), resolve)
  })
}
