import { CacheManager } from './cache.js'
import { Handler } from './handler.js'
import './lib/zip.js'
import './lib/deflate.js'
import './lib/inflate.js'

zip.useWebWorkers = false

const _self = (self as any) as ServiceWorkerGlobalScope

const BASE_URL = process.env.LIANXI_BASE_URL + '/sw' ?? '/sw'

const cacheManager = CacheManager.create()
const handler = cacheManager.then((manager) => new Handler(manager))

_self.addEventListener('install', (event) => {
  event.waitUntil(cacheManager.then((manager) => manager.installCache()))
})

_self.addEventListener('activate', (event) => {
  event.waitUntil(_self.clients.claim())
})

_self.addEventListener('fetch', (event) => {
  event.respondWith(dispatch(event.request))
})

async function dispatch(request: Request): Promise<Response> {
  const url = new URL(request.url)

  if (!url.pathname.startsWith(BASE_URL)) {
    return (await handler).getDefault(request)
  }

  switch (url.pathname.substring(BASE_URL.length)) {
    case '/exercise': {
      const id = url.searchParams.get('id')
      if (!id) return new Response(null, { status: 400 })
      return (await handler).getExercise(id)
    }

    case '/exercise-resource': {
      const id = url.searchParams.get('id')
      const filename = url.searchParams.get('filename')
      if (!id || !filename) return new Response(null, { status: 400 })
      return (await handler).getExerciseResource(id, filename)
    }

    case '/material': {
      const id = url.searchParams.get('id')
      if (!id) return new Response(null, { status: 400 })
      return (await handler).getMaterial(id)
    }

    case '/material-resource': {
      const id = url.searchParams.get('id')
      const page = url.searchParams.get('page')
      if (!id || !page) return new Response(null, { status: 400 })
      return (await handler).getMaterialResource(id, page)
    }

    case '/clear-caches':
      return (await handler).postClearCaches()

    default:
      return new Response(null, { status: 404 })
  }
}
