import { getEntryBlob } from './zip'

export const BASE_URL = process.env.LIANXI_BASE_URL ?? ''

export class CacheManager {
  static async create(): Promise<CacheManager> {
    return new CacheManager(
      await caches.open('static'),
      await caches.open('exercises'),
      await caches.open('materials'),
      await caches.open('metadata')
    )
  }

  private constructor(
    private readonly staticCache: Cache,
    private readonly exercisesCache: Cache,
    private readonly materialsCache: Cache,
    private readonly metadataCache: Cache
  ) {}

  async installCache(): Promise<void> {
    // return this.staticCache.addAll([
    //   BASE_URL + '/',
    //   BASE_URL + '/index.js',
    //   BASE_URL + '/index.css',
    //   CDN_BASE_URL + 'antd/dist/antd.min.css',
    //   CDN_BASE_URL + 'react/umd/react.production.min.js',
    //   CDN_BASE_URL + 'react-dom/umd/react-dom.production.min.js',
    //   CDN_BASE_URL + 'tinycolor2/dist/tinycolor-min.js',
    //   CDN_BASE_URL + 'antd/dist/antd.min.js'
    // ])
  }

  getStatic(request: Request): Promise<Response | undefined> {
    return this.staticCache.match(request)
  }

  getExerciseResource(
    id: string,
    filename: string
  ): Promise<Response | undefined> {
    return this.exercisesCache.match(`/${id}/${filename}`)
  }

  getMaterial(id: string): Promise<Response | undefined> {
    return this.metadataCache.match(`/material/${id}`)
  }

  getMaterialResource(
    id: string,
    filename: string
  ): Promise<Response | undefined> {
    return this.materialsCache.match(`/${id}/${filename}`)
  }

  async cacheExerciseResource(id: string, entry: zip.Entry): Promise<void> {
    const type = getFileType(entry.filename)
    if (!type) return
    return this.exercisesCache.put(
      `/${id}/${entry.filename}`,
      new Response(await getEntryBlob(entry, type), {
        headers: { 'Content-Type': type }
      })
    )
  }

  async cacheMaterial(id: string, content: string): Promise<Response> {
    const response = new Response(content, {
      headers: { 'Content-Type': 'text/plain' }
    })
    await this.metadataCache.put(`/material/${id}`, response.clone())
    return response
  }

  async cacheMaterialResource(id: string, entry: zip.Entry): Promise<void> {
    const type = getFileType(entry.filename)
    if (!type) return
    return this.materialsCache.put(
      `/${id}/${entry.filename}`,
      new Response(await getEntryBlob(entry, type), {
        headers: { 'Content-Type': type }
      })
    )
  }

  async clearCaches(): Promise<void> {
    await Promise.all([
      caches.delete('static'),
      caches.delete('exercises'),
      caches.delete('materials'),
      caches.delete('metadata')
    ])
  }
}

function getFileType(filename: string): string | undefined {
  const extension = filename.substring(filename.lastIndexOf('.') + 1)
  switch (extension) {
    case 'html':
      return 'text/html'
    case 'png':
      return 'image/png'
  }
}
