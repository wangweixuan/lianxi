import { CacheManager } from './cache'
import { createZipReader, getEntryBlob, getReaderEntries } from './zip'

const API_EXERCISE =
  'https://edu.learnonly.com/api/exercise/app/question/getOfflineZip?size=24&exerciseid='

const API_MATERIAL = 'https://edu.learnonly.com/api/v1/dx/downloadDx?dxid='

export class Handler {
  constructor(private readonly cacheManager: CacheManager) {}

  async getExercise(id: string): Promise<Response> {
    const cachedResponse = await this.cacheManager.getExerciseResource(
      id,
      id + '.html'
    )
    if (cachedResponse) {
      return cachedResponse
    }

    const response = await fetch(API_EXERCISE + id)
    const reader = await createZipReader(
      new zip.BlobReader(await response.blob())
    )
    const entries = await getReaderEntries(reader)
    await Promise.all(
      entries.map(async (entry) =>
        this.cacheManager.cacheExerciseResource(id, entry)
      )
    )

    const theEntry = entries.find((entry) => entry.filename === id + '.html')
    if (!theEntry) {
      return new Response(null, { status: 500 })
    }
    const theResponse = new Response(await getEntryBlob(theEntry, 'text/html'))
    reader.close()

    return theResponse
  }

  async getExerciseResource(id: string, filename: string): Promise<Response> {
    return (
      (await this.cacheManager.getExerciseResource(id, filename)) ??
      new Response(null, { status: 500 })
    )
  }

  async getMaterial(id: string): Promise<Response> {
    const cachedResponse = await this.cacheManager.getMaterial(id)
    if (cachedResponse) {
      return cachedResponse
    }

    const response = await fetch(API_MATERIAL + id)
    const reader = await createZipReader(
      new zip.BlobReader(await response.blob())
    )
    const entries = await getReaderEntries(reader)
    await Promise.all(
      entries.map(async (entry) =>
        this.cacheManager.cacheMaterialResource(id, entry)
      )
    )

    return this.cacheManager.cacheMaterial(id, entries.length.toString())
  }

  async getMaterialResource(id: string, page: string): Promise<Response> {
    return (
      (await this.cacheManager.getMaterialResource(id, `page-${page}.png`)) ??
      new Response(null, { status: 500 })
    )
  }

  async postClearCaches(): Promise<Response> {
    await this.cacheManager.clearCaches()
    return new Response()
  }

  async getDefault(request: Request): Promise<Response> {
    const cachedResponse = await this.cacheManager.getStatic(request)
    if (cachedResponse) {
      return cachedResponse
    }

    const response = await fetch(request)
    return response ?? new Response(null, { status: 404 })
  }
}
