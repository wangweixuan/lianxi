/* eslint-env node */

import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import replace from '@rollup/plugin-replace'
import typescript from '@rollup/plugin-typescript'
import { terser } from 'rollup-plugin-terser'

const isProduction = process.env.NODE_ENV === 'production'

export default [
  {
    input: 'src/index.tsx',
    output: {
      dir: 'public',
      format: 'iife',
      globals: {
        'antd': 'antd',
        'react': 'React',
        'react-dom': 'ReactDOM'
      }
    },
    external: ['antd', 'react', 'react-dom'],
    plugins: [
      resolve(),
      commonjs(),
      replace({
        'process.env.NODE_ENV': `"${process.env.NODE_ENV}"`,
        'process.env.LIANXI_BASE_URL': `"${process.env.LIANXI_BASE_URL || ''}"`
      }),
      typescript({ tsconfig: 'src/tsconfig.json' }),
      ...(isProduction ? [terser({ output: { comments: false } })] : [])
    ]
  },
  {
    input: 'src/worker/sw.ts',
    output: {
      dir: 'public'
    },
    plugins: [
      resolve(),
      commonjs(),
      typescript({ tsconfig: 'src/worker/tsconfig.json' }),
      replace({
        'process.env.LIANXI_BASE_URL': `"${process.env.LIANXI_BASE_URL || ''}"`
      }),
      ...(isProduction ? [terser({ output: { comments: false } })] : [])
    ]
  }
]
