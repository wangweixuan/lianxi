'use-strict'
//@ts-check
/* eslint-env node */

import { dest, parallel, src } from 'gulp'

const isProduction = process.env.NODE_ENV === 'production'

export async function assets() {
  return src('./assets/*').pipe(dest('./public/assets'))
}

export async function html() {
  const htmlmin = (await import('gulp-htmlmin')).default
  const replace = (await import('gulp-replace')).default

  let stream = src('./src/index.html')
  if (isProduction) {
    stream = stream.pipe(htmlmin({ collapseWhitespace: true }))
  } else {
    stream = stream
      .pipe(replace('.production', '.development'))
      .pipe(replace('.min', ''))
  }
  return stream.pipe(dest('./public'))
}

export async function styles() {
  const cssnano = (await import('cssnano')).default
  const postcss = (await import('gulp-postcss')).default

  return src('./src/index.css')
    .pipe(postcss([cssnano]))
    .pipe(dest('./public'))
}

export async function scripts() {
  const rollup = await import('rollup')
  const configs = (await import('./rollup.config')).default

  return Promise.all(
    configs.map((config) =>
      rollup.rollup(config).then((bundle) => bundle.write(config.output))
    )
  )
}

export async function vendor() {
  const { promises: fs } = await import('fs')

  try {
    await fs.mkdir('./public/vendor', { recursive: true })
  } finally {
  }

  return Promise.all([
    fs.cp(
      './node_modules/react/umd/react.production.min.js',
      './public/vendor/react.js'
    ),
    fs.cp(
      './node_modules/react-dom/umd/react-dom.production.min.js',
      './public/vendor/react-dom.js'
    ),
    fs.cp('./node_modules/antd/dist/antd.min.js', './public/vendor/antd.js'),
    fs.cp('./node_modules/antd/dist/antd.min.css', './public/vendor/antd.css'),
    fs.cp(
      './node_modules/antd/dist/antd.dark.min.css',
      './public/vendor/antd.dark.css'
    )
  ])
}

export default parallel(assets, html, styles, scripts, vendor)
